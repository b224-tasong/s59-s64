import HeaderNav from './component/HeaderNav/HeaderNav';
import { UserProvider } from './UserContext'

import Home from './pages/home/Home';
import './App.css'

import { useState, useEffect } from 'react';

function App() {

  const [user, setUser] =useState({
      
      id: null,
      isAdmin: null

  });

  const unsetUser = () => {
      localStorage.clear();
  }

  useEffect(() =>{
      fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {
          if (typeof data._id !== "undefined") {

              setUser({
                  id: data._id,
                  isAdmin: data.isAdmin
              })

          }else{
              setUser({
                  id: null,
                  isAdmin: null
              })
          }
      })
  }, [])

  return (
      <UserProvider value={{user, setUser, unsetUser}}>
          <div className='container-fluid backgroundColor'>

            <HeaderNav />
            <Home/>

          </div>
      </UserProvider>
  );
}

export default App;
