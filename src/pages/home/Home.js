import mainBG from '../../img/mainBG2.png';
import './Home.css';
import Register from '../../component/Modal/Register';





export default function Home() {



	return(
			
		<>
			<Register />
			<div className='container home-container'>

				<div className='row'>
					<div className='col-lg-6 text-container'>
						<div className='Home-text-wrapper'>
							<h1>Play Sports With <br/> A New Style!</h1>
							<p>
								Success isn't always about greatness. It's about consistency. Consistent
								hard work gains success. Greatness will come.
							</p>
							<button className='btn' data-bs-toggle="modal" data-bs-target="#exampleModal1">Register</button>
						</div>
					</div>
					<div className='col-lg-6'>
						<div className='Home-background-wrapper'>
							<img src={mainBG} className='main-background-picture' alt="Home Background"/>
						</div>
					</div>
				</div>

			</div>

		</>

		)

}