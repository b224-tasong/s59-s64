import React from 'react';


// Create a Context Object
// A context object as the name suggest is a data type of an object that can be used
// to store information that can be shared to other components within the app.
// Context object is a different approach in passing information between components and allows 
// easier access by avoiding props-drilling
const UserContext = React.createContext();


// Provider component allows other component to consum or use the context object and supply the neccessary information needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;