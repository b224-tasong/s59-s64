import logo from '../../img/logo1.png'
import './HeaderNav.css'
import Login from '../Modal/Login';

export default function HeaderNav() {

	return(
		<>
			<Login />
			<nav className="navbar navbar-expand-sm navbar-light">
				  <div className="container">
					<a className="navbar-brand " href="#"><img src={logo} className="webLogo" alt='Website logo'/></a>
					<button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					  <span className="navbar-toggler-icon"></span>
					</button>
					<div className="collapse navbar-collapse " id="navbarSupportedContent">
					  <ul className="navbar-nav ms-auto">
						<li className="nav-item me-4">
						  <a className="nav-link" aria-current="page" href="#">Home</a>
						</li>
						<li className="nav-item me-4">
						  <a className="nav-link" href="#">About</a>
						</li>
						<li className="nav-item me-4">
						  <a className="nav-link" href="#">Product</a>
						</li>	
						<li className="nav-item">
						 <button className="btn-login" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Login</button>
						</li>				
					  </ul>		  
					</div>
				  </div>
				</nav>
		</>		
		);

};

