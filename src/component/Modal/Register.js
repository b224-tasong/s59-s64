import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';

export default function Register() {


	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	const [isActive, setIsActive] = useState(false);

	function registerUser(e) {
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail/`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if (data === true) {

				Swal.fire({
					title: 'Dulicate Email Found',
					icon: 'error',
					text: 'Please try a different email'
				})
				setEmail('');

			}else{
	
				fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},

					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo

					})
				})
				.then(res => res.json())
				.then(data => {

					if (data === true) {

						// Clearing the input fields and states
						setFirstName('');
						setLastName('');
						setEmail('');
						setPassword1('');
						setPassword2('');
						setMobileNo('');

						Swal.fire({
							title: 'You have successfully registered',
							icon: 'success',
							text: 'Congratulation!'
						})	
						
					}else{
						Swal.fire({
							title: 'Failed to register account',
							icon: 'success',
							text: 'Please try again!'
						})
					}
				})
			}
		})	
	}


	useEffect(()=>{
		if ((firstName !== "" && lastName !== "" && email !== "" && password1 !== "" && password2 !== "" && mobileNo.length === 11) && (password1 === password2)) {
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [firstName, lastName, email, password1, password2, mobileNo]);

	const handleModalClose = () => {
		setFirstName('');
		setLastName('');
		setEmail('');
		setPassword1('');
		setPassword2('');
		setMobileNo('');
		// Close the modal
	} 

	return(

			<>

				<div className="modal fade" id="exampleModal1" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable">
				    <div className="modal-content">
				      <div className="modal-header">
				        <h5 className="modal-title" id="exampleModalLabel">Register account.</h5>
				        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={handleModalClose}></button>
				      </div>
				      <div className="modal-body">
				        <form onSubmit={e => registerUser(e)}>
				        	<div className="mb-3">
				        	  <label htmlFor="exampleFormControlInput1" className="form-label">First Name</label>
				        	  <input type="text" className="form-control" id="exampleFormControlInput1"
				        	  	value={firstName}
				        	  	onChange={e => setFirstName(e.target.value)}
				        	  />
				        	</div>
				        	<div className="mb-3">
				        	  <label htmlFor="exampleFormControlInput1" className="form-label">Last Name</label>
				        	  <input type="text" className="form-control" id="exampleFormControlInput2"
				        	  	value={lastName}
				        	  	onChange={e => setLastName(e.target.value)}
				        	  />
				        	</div>
				         	<div className="mb-3">
				         	  <label htmlFor="exampleFormControlInput1" className="form-label">Email address</label>
				         	  <input type="email" className="form-control" id="exampleFormControlInput3"
				         	  	value={email}
				         	  	onChange={e => setEmail(e.target.value)}
				         	  />
				         	</div>
				         	<div className="mb-3">
				         	  <label htmlFor="exampleFormControlInput1" className="form-label">Mobile Number</label>
				         	  <input type="text" className="form-control" id="exampleFormControlInput4"
				         		 value={mobileNo}
				         		 onChange={e => setMobileNo(e.target.value)}
				         	  />
				         	</div>
				         	<div className="mb-3">
				         	<label htmlFor="exampleFormControlInput1" className="form-label">Password</label>
				         	  <input type="Password" className="form-control" id="exampleFormControlInput5"
				         	   value={password1}
				         	   onChange={e => setPassword1(e.target.value)}
				         	  />
				         	</div>
				         	<div className="mb-3">
				         	  <label htmlFor="exampleFormControlInput1" className="form-label">Verify password</label>
				         	  <input type="Password" className="form-control" id="exampleFormControlInput6"
				         	  	value={password2}
				         	  	onChange={e => setPassword2(e.target.value)}
				         	  />
				         	</div>

				         	<div className="modal-footer">
				         	  <button type="button" className="btn-danger" data-bs-dismiss="modal">Close</button>
				         	  <button type="button" className="btn-primary" onClick={handleModalClose}>Clear</button>
				         	  {
				         	  	isActive ?
				         	  	<button type="button" className="btn-primary" id="submitBbtn" onClick={registerUser}>Send message</button>
				         	  		:
				         	  	<button type="button" className="btn-secondary" disabled>Send message</button>
				         	  }
				         	</div>
				        </form>
				      </div>
				      
				    </div>
				  </div>
				</div>
			</>

		)

}