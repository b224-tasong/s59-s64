import { useState, useEffect, useContext} from 'react';
import UserContext from '../../UserContext.js';
import Swal from 'sweetalert2'

export default function Login() {

	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);


	function loginUser(e) {
		
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{
			// We will receive either a token or a false response.
			console.log(data)

			if (typeof data.tokenAccess  !== "undefined") {
				localStorage.setItem('token', data.access )
				retrieveUserDetails(data.access)
				Swal.fire({
					title: 'Login Successfully',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})
				
			}else{
				Swal.fire({
					title: 'Authentication Failed!',
					icon: 'error',
					text: 'Incorrect Email or Password!'

				})
			}
		})
	}


	const retrieveUserDetails = (token) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers: {
				Authorization: `Bearer ${token}`
			}

		})
		.then(res => res.json())
		.then(data=> {
			console.log(data);
			// Global user state for validataion accross the whole app

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(()=>{
		if (email !== "" && password !== "") {
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password]);

	return(

			<>

				<div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable">
				    <div className="modal-content">
				      <div className="modal-header">
				        <h5 className="modal-title" id="exampleModalLabel">Please login your account.</h5>
				        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				      </div>
				      <div className="modal-body">
				        <form onSubmit={e => loginUser(e)}>
				         	<div className="mb-3">
				         	  <label htmlFor="email" className="form-label">Email address</label>
				         	   <input type="email" className="form-control" id="login-email"
				         	  	value={email}
				         	  	onChange={e => setEmail(e.target.value)}
				         	  />
				         	</div>
				         	<div className="mb-3">
				         	  <label htmlFor="password" className="form-label">Password</label>
				         	  <input type="Password" className="form-control" id="login-password"
				         	  	value={password}
				         	  	onChange={e => setPassword(e.target.value)}
				         	  />
				         	</div>
				         	<div className="modal-footer">
				         	  <button type="button" className="btn-danger" data-bs-dismiss="modal">Close</button>
				         	  <button type="button" className="btn-primary">Clear</button>
				         	  {
				         	  	isActive ?
				         	  	<button type="button" className="btn-primary" onClick={loginUser}>Send message</button>
				         	  		:
				         	  	<button type="button" className="btn-secondary" disabled>Send message</button>
				         	  }
				         	</div>
				        </form>
				      </div>
				    </div>
				  </div>
				</div>
			</>

		)

}